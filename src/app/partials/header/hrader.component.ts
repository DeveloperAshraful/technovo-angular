import { Component, OnInit } from '@angular/core';
import {faBars} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  public isMenuCollapsed = true;

  title = 'TechNovo';
  logo = 'assets/images/logo.png';
  phone = '03301139963';

  bar = faBars;

  constructor() { }

  ngOnInit(): void {
  }

}
