import {Component, OnInit} from '@angular/core';
import {faFacebookF, faLinkedinIn, faTwitter, faInstagram} from '@fortawesome/free-brands-svg-icons';
import {faEnvelope, faPhoneAlt} from '@fortawesome/free-solid-svg-icons';
import {PartnerData} from "../../services/partner/partnerData";

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  partners = PartnerData;

  title = 'Tech Novo';
  logo = 'assets/images/logo.png';
  number = '03301139963';
  fbpage = 'https://www.facebook.com/technovoltd';
  twitterpage = 'https://twitter.com/technovoltd';
  instagrampage = 'https://www.instagram.com/technovoltd/';
  linkedpage = '';
  emailid = 'hello@tech-novo.uk';
  facebook = faFacebookF;
  linkedin = faLinkedinIn;
  twitter = faTwitter;
  email = faEnvelope;
  phone = faPhoneAlt;
  instagram = faInstagram;

  copyright = '<strong>Tech Novo</strong> is a trading name of tek-novo ltd, which is registered in England. Company No: 13019787 – Registered Address: Innovation Centre & Business Base 110 Butterfield, Great Marlings, Luton, Bedfordshire, England, LU2 8DL.';

  constructor() {
  }

  ngOnInit(): void {
  }

}
