import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
// Pages Components
import {PageHomeComponent} from './pages/home/home.component';
import {PageContactComponent} from './pages/contact/contact.component';
import {PageWebDesignComponent} from './pages/webdesign/webdesign.component';
import {PageSocialMarketingComponent} from './pages/socialmarketing/socialmarketing.component';
import {PageProjectsComponent} from './pages/projects/projects.component';
import {PageNotfoundComponent} from './pages/notfound/notfound.component';
import {PageEcommerceComponent} from './pages/ecommerce/ecommerce.component';
import {PageAllinComponent} from './pages/allinone/allinone.component';
import {PageAboutComponent} from './pages/about/about.component';
import {PageSocialCampaignComponent} from './pages/socialcampaign/socialcampaign.component';
import {PageFeedbackComponent} from './pages/feedback/feedback.component';
import {PageBusinessComponent} from './pages/business/business.component';
import {PageOfferComponent} from './pages/offer/offer.component';

const routes: Routes = [
  {
    path: '', redirectTo: '', component: PageHomeComponent, pathMatch: 'full',
    data: {
      title: 'Tech-Novo,Your Trusted Digital Partner',
      description: 'Website, Facebook Marketing, graphics design from only £99',
      robots: 'index, follow',
      ogTitle: 'Website, Facebook Marketing, graphics design from only £99'
    }
  },
  {
    path: 'about-us', component: PageAboutComponent,
    data: {
      title: 'About Us',
      description: 'We do Social marketing, branding, website design, e-commerce & development',
      robots: 'index, follow',
      ogTitle: 'We do Social marketing, branding, website design, e-commerce & development'
    }
  },
  {
    path: 'web-design', component: PageWebDesignComponent,
    data: {
      title: 'Web Design from £99 ',
      description: 'Wed design, e-commerce, online ordering system from £99',
      robots: 'index, follow',
      ogTitle: 'DWed design, e-commerce, online ordering system from £99'
    }
  },
  {
    path: 'social-media-pricing', component: PageSocialMarketingComponent,
    data: {
      title: 'Social marketing from £99',
      description: 'Facebook marketing with free boosting from £99',
      robots: 'index, follow',
      ogTitle: 'Facebook marketing with free boosting from £99'
    }
  },
  {
    path: 'ecommerce', component: PageEcommerceComponent,
    data: {
      title: 'Online Ordering platform ',
      description: 'Start to sell online from £75, for restaurants and takeaways',
      robots: 'noindex, nofollow',
      ogTitle: 'Description of Home Component for social media'
    }
  },
  {
    path: 'epos-cloud-solutions', component: PageAllinComponent,
    data: {
      title: 'Epos Cloud Solutions',
      description: '6 Description of Home Component',
      robots: 'index, follow',
      ogTitle: 'Description of Home Component for social media'
    }
  },
  {
    path: 'projects', component: PageProjectsComponent,
    data: {
      title: 'Projects',
      description: '7 Description of Home Component',
      robots: 'index, follow',
      ogTitle: 'Description of Home Component for social media'
    }
  },
  {
    path: 'contact-us', component: PageContactComponent,
    data: {
      title: 'Contact Us',
      description: '8 Description of Home Component',
      robots: 'noindex, nofollow',
      ogTitle: 'Description of Home Component for social media'
    }
  },
  {
    path: 'social-media-restaurant-takeaways', component: PageSocialCampaignComponent,
    data: {
      title: 'Social Media post for  Restaurant Takeaways',
      description: 'Facebook post for restaurants and takeaways',
      robots: 'index, follow',
      ogTitle: 'Facebook post for restaurants and takeaways'
    }
  },
  {
    path: 'others-business', component: PageBusinessComponent,
    data: {
      title: 'Facebook post for Business',
      description: 'Facebook posting for other business',
      robots: 'index, follow',
      ogTitle: 'Facebook posting for other business'
    }
  },

  // Customer Feedback Disabled Enbaled
  // {
  //   path: 'customer-feedback', component: PageFeedbackComponent,
  //   data: {
  //     title: 'Customer Feedback',
  //     description: 'Contact us by email',
  //     robots: 'index, follow',
  //     ogTitle: 'Contact us by email'
  //   }
  // },
  {
    path: 'free-offers', component: PageOfferComponent,
    data: {
      title: 'Free Offers',
      description: 'Free Offers',
      robots: 'index, follow',
      ogTitle: 'Free Offers'
    }
  },
  {
    path: '**', component: PageNotfoundComponent,
    data: {
      title: 'Nothing Found',
      description: 'Sorry we could not find anything',
      robots: 'noindex, nofollow',
      ogTitle: 'Sorry we could not find anything'
    }
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: false})],
  exports: [RouterModule],
  declarations: []
})

export class RoutingModule {
}
