import { BrowserModule, Title, Meta } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
// AnimationsModule
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// App Component
import { AppComponent } from './app.component';
// Route Component
import { RoutingModule } from './routing.module';
// Bootstrap
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
// Owl Carousel
import {CarouselModule} from 'ngx-owl-carousel-o';
// FontAwesome
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
// Particles
import { NgParticlesModule } from 'ng-particles';
//  FormsModule
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
// Environment
import { environment } from '../environments/environment';
// HttpModule
import { HttpClientModule } from "@angular/common/http";
// Google Maps Agm
import { AgmCoreModule } from '@agm/core';
// Partials Components
import { HeaderComponent } from './partials/header/hrader.component';
import { FooterComponent } from './partials/footer/footer.component';
// Pages Components
import { PageHomeComponent } from './pages/home/home.component';
import { PageWebDesignComponent } from './pages/webdesign/webdesign.component';
import { PageSocialMarketingComponent } from './pages/socialmarketing/socialmarketing.component';
import { PageProjectsComponent } from './pages/projects/projects.component';
import { PageContactComponent } from './pages/contact/contact.component';
import { PageEcommerceComponent } from './pages/ecommerce/ecommerce.component';
import { PageSocialCampaignComponent } from './pages/socialcampaign/socialcampaign.component';
import { PageAboutComponent } from './pages/about/about.component';
import { PageAllinComponent } from './pages/allinone/allinone.component';
import { PageFeedbackComponent } from './pages/feedback/feedback.component';
import { PageBusinessComponent } from './pages/business/business.component';
import { PageOfferComponent } from './pages/offer/offer.component';
import { PageNotfoundComponent } from './pages/notfound/notfound.component';

// Components
import { SliderComponent } from './components/slider/slider.component';
import { ProgressComponent } from './components/progress/progress.component';
import { ServicesComponent } from './components/services/services.component';
import { WhyChooseComponent } from './components/choose/choose.component';
import { CallactionComponent } from './components/callaction/callaction.component';
import { NewsComponent } from './components/news/news.component';
import { ProjectComponent } from './components/project/project.component';
import { ContentComponent } from './components/content/content.component';
import { BannerComponent } from './components/banner/banner.component';
import { QuoteComponent } from './components/quote/quote.component';
import { PricelistComponent } from './components/price/price.component';
import { FormComponent } from './components/form/form.component';
import { ComingsoonComponent } from './components/comingsoon/comingsoon.component';
import { CartmethodComponent } from './components/cartgmethod/cartmethod.component';
import { ShareholderComponent } from './components/shareholder/shareholder.component';
import { FeatureComponent } from './components/feature/feature.component';
import { CampaignComponent } from './components/campaign/campaign.component';
import { FeedbackComponent } from './components/feedback/feedback.component';
import { PartnerComponent } from './components/partner/partner.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    PageHomeComponent,
    PageWebDesignComponent,
    PageSocialMarketingComponent,
    PageProjectsComponent,
    PageEcommerceComponent,
    PageContactComponent,
    PageAboutComponent,
    PageSocialCampaignComponent,
    PageAllinComponent,
    PageNotfoundComponent,
    PageFeedbackComponent,
    PageBusinessComponent,
    PageOfferComponent,
    SliderComponent,
    ProgressComponent,
    ServicesComponent,
    WhyChooseComponent,
    CallactionComponent,
    NewsComponent,
    ProjectComponent,
    ContentComponent,
    BannerComponent,
    QuoteComponent,
    PricelistComponent,
    FormComponent,
    ComingsoonComponent,
    CartmethodComponent,
    ShareholderComponent,
    FeatureComponent,
    CampaignComponent,
    FeedbackComponent,
    PartnerComponent
  ],
  imports: [
    BrowserModule,
    RoutingModule,
    NgbModule,
    HttpClientModule,
    FontAwesomeModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    NgParticlesModule,
    CarouselModule,
    AgmCoreModule.forRoot({
      apiKey: environment.mapURL
    })
  ],
  providers: [Title,Meta],
  bootstrap: [AppComponent]
})
export class AppModule { }
