import {Price} from './price';

export const SocialPrice: Price[] = [
  {
    price_id: 1,
    price_girdClass: 'col-lg-6 col-md-6 col-sm-6',
    price_class: 'social-price-item',
    price_heading: 'Standard',
    price_sub: '(Startup)',
    price_from: '',
    price_currency: '£',
    price_price: '99',
    price_offer: '',
    price_type: ' / Month',
    price_featured: [
      {
        heading: 'Facebook',
        heading_type: 'Y'
      },
      {
        heading: 'Instagram',
        heading_type: 'Y'
      },
      {
        heading: 'Organic Campaign',
        heading_type: 'Y'
      },
      {
        heading: 'Facebook',
        heading_type: '2/Week'
      },
      {
        heading: 'Instagram',
        heading_type: '2/Week'
      },
      {
        heading: '<b>Free Facebook  Boosting</b>',
        heading_type: 'Y'
      },
      {
        heading: 'Video Promotion',
        heading_type: 'N'
      },
      {
        heading: 'Unique Campaign',
        heading_type: 'Y'
      },
      {
        heading: '24/7 Customer Support',
        heading_type: 'Y'
      },
      {
        heading: 'Local Customer Target',
        heading_type: 'Y'
      },
      {
        heading: 'National Customer Target',
        heading_type: 'Y'
      }
    ],
    price_button: '',
    price_url: '',
    price_btn: 'Contact Us',
    price_link: '/contact-us',
    price_img: '',
    price_more: '',
    price_collapse: ''
  },
  {
    price_id: 2,
    price_girdClass: 'col-lg-6 col-md-6 col-sm-6',
    price_class: 'social-price-item',
    price_heading: 'Advance',
    price_sub: '(Boost Your Business)',
    price_from: '',
    price_currency: '£',
    price_price: '149',
    price_offer: '',
    price_type: '/ month',
    price_featured: [
      {
        heading: 'Facebook Management',
        heading_type: 'Y',
      },
      {
        heading: 'Instagram Management',
        heading_type: 'Y',
      },
      {
        heading: 'Organic Campaign',
        heading_type: 'Y'
      },
      {
        heading: 'Facebook',
        heading_type: '3/Week'
      },
      {
        heading: 'Instagram',
        heading_type: '3/Week'
      },

      {
        heading: '<b>Free Facebook  Boosting</b>',
        heading_type: 'Y'
      },
      {
        heading: '<b>Video Promotion</b>',
        heading_type: '1/Month'
      },
      {
        heading: 'Unique Campaign',
        heading_type: 'Y'
      },
      {
        heading: '24/7 Customer Support',
        heading_type: 'Y'
      },
      {
        heading: 'Local Customer Target',
        heading_type: 'Y'
      },
      {
        heading: 'National Customer Target',
        heading_type: 'Y'
      }
    ],
    price_button: '',
    price_url: '',
    price_btn: 'Contact Us',
    price_link: '/contact-us',
    price_img: '',
    price_more: '',
    price_collapse: ''
  }

];
