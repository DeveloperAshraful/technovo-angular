export interface Price {
  price_id: number;
  price_girdClass: string;
  price_class: string;
  price_heading: string;
  price_sub: string;
  price_from: string;
  price_currency: string;
  price_price: string;
  price_offer: string;
  price_type: string;
  price_featured: any[];
  price_button: string;
  price_url: string;
  price_btn: string;
  price_link: string;
  price_img: string;
  price_more: string;
  price_collapse: string;
}
