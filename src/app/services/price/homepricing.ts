import {Price} from './price';

export const HomePricing: Price[] = [
  {
    price_id: 1,
    price_girdClass: 'col-lg-4 col-md-6 col-sm-6',
    price_class: 'yuma-home-price',
    price_heading: 'Social Media Marketing',
    price_sub: '',
    price_from: 'From',
    price_currency: '£',
    price_price: '99',
    price_offer: '',
    price_type: '/m',
    price_featured: [
      {heading: 'Beautifully designed Facebook Page'},
      {heading: 'Draw more customer with stunning post'},
      {heading: 'Increase page reputation '},
      {heading: 'Reach the audience that matters most'},
      {heading: 'Free video promotion'},
      {heading: 'Free logo design '},
      {heading: 'Improving communication and interaction'}
    ],
    price_button: '',
    price_url: '',
    price_btn: 'Learn More',
    price_link: '/social-media-pricing',
    price_img: 'assets/images/marketing.png',
    price_more: '',
    price_collapse: ''
  },
  {
    price_id: 2,
    price_girdClass: 'col-lg-4 col-md-6 col-sm-6',
    price_class: 'yuma-home-price',
    price_heading: 'Web Design',
    price_sub: '',
    price_from: 'Only',
    price_currency: '£',
    price_price: '99',
    price_offer: '',
    price_type: '',
    price_featured: [
      {heading: '4 professional  pages'},
      {heading: 'FREE Facebook Boosting'},
      {heading: 'FREE  hosting'},
      {heading: 'FREE UK Domain'},
      {heading: 'FREE Logo design'},
      {heading: 'FREE SSL Certificate'},
      {heading: 'Search engine optimization (SEO)'}
    ],
    price_button: '',
    price_url: '',
    price_btn: 'Learn More',
    price_link: '/web-design',
    price_img: 'assets/images/web.png',
    price_more: '',
    price_collapse: ''
  },
  {
    price_id: 3,
    price_girdClass: 'col-lg-4 col-md-6 col-sm-6',
    price_class: 'yuma-home-price',
    price_heading: 'Online Ordering System',
    price_sub: '',
    price_from: 'From',
    price_currency: '£',
    price_price: '75',
    price_offer: '',
    price_type: '/m',
    price_featured: [
      {heading: 'FREE Website'},
      {heading: 'Android & Apple App'},
      {heading: 'Customer ordering/shopping platform'},
      {heading: 'EPOS integration'},
      {heading: 'Designed  for Restaurants and Takeaway\'s'},
      {heading: 'Delivery tracking and booking '},
      {heading: 'Logo, Products, leaflet design'}
    ],
    price_button: '',
    price_url: '',
    price_btn: 'Learn More',
    price_link: '/contact-us',
    price_img: 'assets/images/partners.png',
    price_more: '',
    price_collapse: ''
  }
];
