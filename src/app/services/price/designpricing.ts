import {Price} from './price';

export const DesignPrice: Price[] = [
  {
    price_id: 1,
    price_girdClass: 'col-lg-4 col-md-6 col-sm-6',
    price_class: '',
    price_heading: 'Standard',
    price_sub: 'Startup',
    price_from: 'Only  ',
    price_currency: '£',
    price_price: '99',
    price_offer: '149',
    price_type: '',
    price_featured: [
      {heading: '4 professional  pages'},
      {heading: 'FREE hosting'},
      {heading: 'FREE UK domain '},
      {heading: 'FREE SSL Certificate'},
      {heading: 'FREE Facebook boosting'},
      {heading: '2 Product image creation'},
      {heading: 'FREE Logo Design', close: 'close'},
      {heading: 'FREE Leaflate Design', close: 'close'}
    ],
    price_button: 'Read more',
    price_url: '/free-offers',
    price_btn: 'Contact Us',
    price_link: '/contact-us',
    price_img: '',
    price_more: 'show',
    price_collapse: ''
  },
  {
    price_id: 2,
    price_girdClass: 'col-lg-4 col-md-6 col-sm-6',
    price_class: 'yuma-price-bg',
    price_heading: 'Gold',
    price_sub: 'Medium-sized business',
    price_from: '',
    price_currency: '£',
    price_price: '249',
    price_offer: '',
    price_type: '',
    price_featured: [
      {heading: '10 professional  pages'},
      {heading: 'FREE hosting'},
      {heading: 'FREE UK domain '},
      {heading: 'FREE SSL Certificate'},
      {heading: 'FREE Facebook boosting'},
      {heading: '6 Product image creation'},
      {heading: 'FREE Logo Design'},
      {heading: 'FREE Leaflate Design', close: 'close'}
    ],
    price_button: 'Read more',
    price_url: '/free-offers',
    price_btn: 'Contact Us',
    price_link: '/contact-us',
    price_img: '',
    price_more: 'show',
    price_collapse: ''
  },
  {
    price_id: 3,
    price_girdClass: 'col-lg-4 col-md-6 col-sm-6',
    price_class: '',
    price_heading: 'Platinum',
    price_sub: 'Larger business',
    price_from: 'From',
    price_currency: '£',
    price_price: '349',
    price_offer: '',
    price_type: '',
    price_featured: [
      {heading: 'Maximum number of pages'},
      {heading: 'FREE hosting'},
      {heading: 'FREE UK domain '},
      {heading: 'FREE SSL Certificate'},
      {heading: 'FREE Facebook boosting'},
      {heading: '15 Product image creation'},
      {heading: 'FREE Logo Design'},
      {heading: 'FREE Leaflate Design'}
    ],
    price_button: 'Read more',
    price_url: '/free-offers',
    price_btn: 'Contact Us',
    price_link: '/contact-us',
    price_img: '',
    price_more: 'show',
    price_collapse: ''
  }
];
