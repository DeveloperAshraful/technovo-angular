import {Offer} from './offer';

export const OfferData: Offer[] = [
  {
    offer_id: 1,
    offer_title: 'FREE Hosting',
    offer_desc: 'All Web design packages come with 1 Year of Free hosting.'
  },
  {
    offer_id: 2,
    offer_title: 'FREE UK Domain',
    offer_desc: 'Web and E-commerce packages come with 1 FREE UK domain, FREE for the first year only.'
  },
  {
    offer_id: 3,
    offer_title: 'FREE SSL Certificate',
    offer_desc: '1 Year FREE SSL certificate for www domain.'
  },
  {
    offer_id: 4,
    offer_title: 'FREE Facebook Boosting',
    offer_desc: 'We will provide FREE Facebook boosting for a month. This has the potential to reach 500 plus people.'
  },
  {
    offer_id: 5,
    offer_title: 'Product image creation',
    offer_desc: 'Our designers will create images to enhance and highlight your products/services. This will help rebrand your business online or on social platforms.'
  },
  {
    offer_id: 6,
    offer_title: 'FREE Logo Design',
    offer_desc: 'Our design team will help create a logo to reflect your business brand.'
  },
  {
    offer_id: 7,
    offer_title: 'FREE leaflet design',
    offer_desc: 'We are offering FREE leaflet design to help you advertise your business and boost sales.'
  }
]
