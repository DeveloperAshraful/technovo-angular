export interface Offer {
  offer_id: number;
  offer_title: string;
  offer_desc: string;
}
