import {Choose} from './choose';

export const ChooseData: Choose[] = [
  {
    choose_id: 1,
    choose_title: 'True Digital Partner',
    choose_desc: 'We understand how frustrating it could be to liaise with multiple IT companies. At tech-novo we can assist you with social marketing, web designing, and portfolio branding all under one roof.',
    choose_img: 'assets/images/partner.png'
  },
  {
    choose_id: 2,
    choose_title: 'Dedicated Consultant ',
    choose_desc: 'A consultant will be assigned as a single point of contact, who will oversee and assist with all requirements from social marketing, technical support, designing and branding. ',
    choose_img: 'assets/images/branding.png'
  },
  {
    choose_id: 3,
    choose_title: 'Teams of Expert Developers',
    choose_desc: 'We have a vibrant, young team of expert developers who can bring fresh new ideas to add value to your business. Our experts will help you reduce your operation time, thus making your business more efficient.',
    choose_img: 'assets/images/developer.png'
  },
  {
    choose_id: 4,
    choose_title: 'Design & Re-branding',
    choose_desc: 'Branding is significant for any business and a good brand always sells itself. Our expert team of designers will assist in portfolio branding, product images, logo design, creating marketing materials, and source printing to deal with all your needs.',
    choose_img: 'assets/images/design-print.png'
  }
];
