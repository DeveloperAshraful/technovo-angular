import { TestBed } from '@angular/core/testing';

import { ChooseService } from './choose.service';

describe('ChooseService', () => {
  let service: ChooseService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ChooseService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
