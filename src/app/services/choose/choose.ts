export interface Choose {
  choose_id: number;
  choose_title: string;
  choose_desc: string;
  choose_img: string;
}
