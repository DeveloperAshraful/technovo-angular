
export let ProjectBanner = {
  title: 'Projects',
  contents: [
    { featured: 'Unique Design'},
    { featured: 'Shopping Cart Integration'},
    { featured: 'Payment Module Integration'}
  ],
  banner_form: 'show',
  banner_image: ''
};
