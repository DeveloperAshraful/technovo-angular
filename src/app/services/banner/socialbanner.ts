
export let SocialBanner = {
  title: 'Social Media Pricing',
  contents: [
    { featured: 'Monthly FREE Boosting'},
    { featured: 'Facebook, Instagram, Google Ads'},
    { featured: 'Stunning product design '}
  ],
  banner_form: '',
  banner_image: 'assets/images/social-campaign.png'
};
