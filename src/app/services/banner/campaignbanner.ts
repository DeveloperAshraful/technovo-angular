
export let CampaignBanner = {
  title: 'Social media benefits for restaurants and takeaways',
  contents: [
    { featured: 'Promoting your products and services'},
    { featured: 'Growing your business awareness'},
    { featured: 'Increasing your sales'},
    { featured: 'Target local customers'}
  ],
  banner_form: 'show',
  banner_image: ''
};
