
export let AllinoneBanner = {
  title: 'All in One Payment System',
  contents: [
    { featured: 'Cloud base order management system'},
    { featured: 'E-commerce and mobile application'},
    { featured: 'Integrated EPOS system'},
    { featured: 'Waiter, kitchen , customer ordering app'}
  ],
  banner_form: 'hide',
  banner_image: 'assets/images/allinone.png'
};
