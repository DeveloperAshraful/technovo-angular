import {Slide} from './slide';

export const SlideData: Slide[] = [
  {
    slide_id: 1,
    slide_title: 'Start your journey with Only £99',
    slide_contents: [
      { heading: 'Facebook campaign - Just £99' },
      { heading: 'Website Design - Only £99' },
      { heading: 'Start to sell online  - from £75/m' }
    ],
    slide_btn: 'Contact Us',
    slide_btn_url: '/contact-us',
    slide_image: 'assets/images/rubisqube.png',
    slide_bg: ''
  },
  {
    slide_id: 3,
    slide_title: 'Grow your Business with our <br> Professional Services',
    slide_contents: [
      { heading: 'Business application development' },
      { heading: 'E-Sign portal' },
      { heading: 'Continuous IT service' }
    ],
    slide_btn: 'Contact Us',
    slide_btn_url: '/contact-us',
    slide_image: 'assets/images/business.png',
    slide_bg: ''
  },
  {
    slide_id: 2,
    slide_title: 'Cloud and Computing Solution',
    slide_contents: [
      { heading: 'Expert IT support' },
      { heading: 'Cloud base hosting' },
      { heading: 'Security & Automation' }
    ],
    slide_btn: 'Contact Us',
    slide_btn_url: '/contact-us',
    slide_image: 'assets/images/cloud.png',
    slide_bg: ''
  }

];
