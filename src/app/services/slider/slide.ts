export interface Slide {
  slide_id: number;
  slide_title: string;
  slide_contents: any[];
  slide_btn: string;
  slide_btn_url: string;
  slide_image: string;
  slide_bg: string;
}
