import {Progress} from './progress';

export const ProgressData: Progress[] = [
  {
    progress_id: 1,
    progress_title: 'Design',
    progress_content: 'First step of a successful application is to create a base design, we will work closely with your team to implement a prototype of work.',
    progress_btn: 'Read More',
    progress_btn_url: ''
  },
  {
    progress_id: 2,
    progress_title: 'Develop',
    progress_content: 'Our expert team will work to create it live. Before finalizing a product we always take feedback from every customer, no matter how many changes we have to perform to build it flawlessly.',
    progress_btn: 'Read More',
    progress_btn_url: ''
  },
  {
    progress_id: 3,
    progress_title: 'Promote',
    progress_content: 'Our social marketing team will work with you to design a brand of your product, present a portfolio  to increase your audience by using social media',
    progress_btn: 'Read More',
    progress_btn_url: ''
  },
  {
    progress_id: 4,
    progress_title: 'Support',
    progress_content: 'Our customers love our support !!, we will be with you 24/7, no matter what time of the day. You can reach us any time at  03301139963, we will be delighted to help',
    progress_btn: 'Read More',
    progress_btn_url: ''
  }
];
