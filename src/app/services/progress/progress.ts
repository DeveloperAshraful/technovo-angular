export interface Progress {
  progress_id: number;
  progress_title: string;
  progress_content: string;
  progress_btn: string;
  progress_btn_url: string;
}
