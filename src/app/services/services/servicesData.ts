import {Services} from './services';

export const ServicesData: Services[] = [
  {
    service_id: 1,
    service_title: 'Social Media Marketing',
    service_excerpt: 'Platforms that will help you connect and  increase revenues ',
    service_desc: 'Unique images in every post, Unique images in every post',
    service_image: 'assets/images/social.png',
    service_url: '/social-media-pricing'
  },
  {
    service_id: 2,
    service_title: 'Web & E-Commerce',
    service_excerpt: 'Reach a wider customer base and improve the client experience through efficiency\n',
    service_desc: 'Unique Design , no template, Unique Design , no template',
    service_image: 'assets/images/ecommerce.png',

    service_url: '/ecommerce'
  },
  {
    service_id: 3,
    service_title: 'Online Ordering Platform',
    service_excerpt: 'Give your customer a way to buy online,for restaurants and takeaway.',
    service_desc: 'Enhance product & service to a wider audience',
    service_image: 'assets/images/epos.png',
    service_url: ''
  }
];
