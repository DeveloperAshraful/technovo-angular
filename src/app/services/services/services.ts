export interface Services {
  service_id: number;
  service_title: string;
  service_excerpt: string;
  service_desc: string;
  service_image: string;
  service_url: string;
}
