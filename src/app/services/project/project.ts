export interface Project {
  project_id: number;
  project_title: string;
  project_cat: string;
  project_url: string;
  project_desc: string;
  project_image: string;
  project_target: string;
}
