import {Project} from './project';

export const ProjectData: Project[] = [
  {
    project_id: 1,
    project_title: 'Novopay',
    project_cat: 'Web Development',
    project_url: 'https://novopay.uk/',
    project_desc: '',
    project_image: 'assets/images/novopay.png',
    project_target: 'new'
  },
  {
    project_id: 2,
    project_title: 'Flava',
    project_cat: 'E-Commerse',
    project_url: '',
    project_desc: '',
    project_image: 'assets/images/flava.png',
    project_target: ''
  },
  {
    project_id: 3,
    project_title: 'Social Media Campaign',
    project_cat: 'Marketing',
    project_url: 'social-media-restaurant-takeaways',
    project_desc: '',
    project_image: 'assets/images/social-media.png',
    project_target: ''
  },
  {
    project_id: 4,
    project_title: 'UstadhLink',
    project_cat: 'Mobile Development',
    project_url: 'https://ustadhlink.com/',
    project_desc: '',
    project_image: 'assets/images/ustadhlink.png',
    project_target: 'new'
  }
];
