import {Partner} from "./partner";

export const PartnerData: Partner [] = [
  {
    partner_id: 1,
    partner_title: 'Novopay',
    partner_url: 'https://novopay.uk/',
    partner_img: 'assets/images/partner/novopay.png'
  },
  {
    partner_id: 2,
    partner_title: 'Evopayments',
    partner_url: 'https://evopayments.co.uk/',
    partner_img: 'assets/images/partner/evo.png'
  }

]


