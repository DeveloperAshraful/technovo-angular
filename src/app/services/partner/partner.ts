export interface Partner {
  partner_id: number;
  partner_title: string;
  partner_url: string;
  partner_img: string;
}
