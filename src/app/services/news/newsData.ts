import {News} from './news';

export const NewsData: News[] = [
  {
    news_id: 1,
    news_title: 'COVID19 help',
    news_excerpt: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
    news_desc: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy',
    news_author: 'Admin',
    news_img: 'assets/images/covid-help.jpg',
    news_btn: 'Read more',
    news_url: '/'
  },
  {
    news_id: 2,
    news_title: 'How to take payments over the phone',
    news_excerpt: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
    news_desc: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy',
    news_author: 'Admin',
    news_img: 'assets/images/payment-phone.jpg',
    news_btn: 'Read more',
    news_url: '/'
  },
  {
    news_id: 3,
    news_title: 'Keeping terminal clean during Covid19',
    news_excerpt: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
    news_desc: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy',
    news_author: 'Admin',
    news_img: 'assets/images/cleaning-terminal.jpg',
    news_btn: 'Read more',
    news_url: '/'
  }
];
