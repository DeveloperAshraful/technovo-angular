export interface News {
  news_id: number;
  news_title: string;
  news_excerpt: string;
  news_desc: string;
  news_author: string;
  news_img: string;
  news_btn: string;
  news_url: string;
}
