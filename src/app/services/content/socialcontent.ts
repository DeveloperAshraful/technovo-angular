
export let SocialContent = {
  heading: 'Let experts help you to increase sell',
  texts: [
    { text: '<p>Delegate all your social media requirements including,  content creation, social media advertising, community engagement, and much more. </p>' },
    { text: '<p>Our expert team can help you define your brand, increase visibility, or generate sales leads.</p>' },
    { text: '<p>Social media is a key component to your success. Tech Novo social media management offers cost-effective, dedicated, ongoing support.</p>' },
    { text: '<p>Also, you have access to our full team of marketing experts including graphic designers, website managers, content creators. Let\'s start to work together!</p>' }
  ],
  items:[],
  content_list: '',
  content_img: 'assets/images/service-list.png'
};
