
export let AllContent = {
  heading: 'Making business simpler',
  texts: [
    { text: '<p>We understand it could be difficult to get all business solutions from one place.</p>' },
    { text: '<p>That’s why we are offering  “All in one product” Epos with cloud base order management system with our partner Novopay.</p>' },
    { text: '<p><strong>Our All-in-one product includes</strong></p>' }
  ],
  items:[
    { item: 'E-Payment gateway'},
    { item: 'SSilky EPOS device.'},
    { item: 'Mobile order system '},
    { item: 'Suitable for any business '},
    { item: 'Online payment '}
  ],
  content_list: '',
  content_img: 'assets/images/service-list.png'
};
