
export let AboutContent = {
  heading: '',
  texts:[
    { text: '<h4>About Tech Novo</h4><p>We are a rapidly growing Web designing & Software company. A digital partner that you can truly and always rely on for all your technological needs from social marketing, branding, web designing, e-commerce and app development. Our mission is to help unleash the potential of every business.</p>' },
    { text: '<h4>Our Team</h4><p>Behind every great achievement, there is a team.  Tech Novo has a young and vibrant team, they understand client needs and work side by side to make sure each business is catered for individually to achieve the best business solution.  At Tech Novo we believe our most powerful tool in the success and growth of our business is team work.</p>' },
    { text: '<h4>Meet team Tech Novo</h4> <p>Web designing team, with a total of 12 years of experience between them</p>' },
    { text: '<h4>Come meet the team</h4>' }
    ],
  items: [
    { item: '<strong>Web designers - </strong> with a total of 12 years of experience between them all, they are here to help you bring eye catching designs in order to bring your product to life.' },
    { item: '<strong>Graphic designers - </strong> with a total of 15 years between them, they are able to add great artistic touch to your brand and creating designs to inspire, inform and captivate your consumers.' },
    { item: '<strong>Social Media team - </strong> Our social media team partnered with the graphic team spend hours trying to achieve the best look for your company.  When it comes to social media campaigns, the teams are perfectionist and will bring your designs to life and attract a wider audience.' },
    { item: '<strong>Development Team - </strong> this team works closely with our client to understand their needs.  Through open and honest communication, the team will guide and consult when necessary and help you better understand requirements.  With our developers at your disposal, you will be able to achieve your business goals with ease.' }
  ],
  content_list: 'list_dot',
  content_img: 'assets/images/service-list.png'
};
