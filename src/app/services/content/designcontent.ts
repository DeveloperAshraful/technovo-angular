
export let DesignContent = {
  heading: 'A professional looking website tells everything',
  texts:[
    { text: '<p>Building customer confidence should be the main focus of a business. A good professional looking websites always give your customer confidence and boost.</p>'},
    { text: '<p>When you build your website, you can also have access to our full team of marketing experts including graphic designers, website managers, content creators, branding managers.</p>'},
    { text: '<p><strong>Our main services includes</strong></p>' }
  ],
  items: [
    { item: 'Social media '},
    { item: 'Mobile app development'},
    { item: 'E-commerce solution'},
    { item: '1-month FREE social media boosting'},
    { item: 'Branding company portofolio'},
    { item: 'Graphic designing '},
  ],
  content_list: '',
  content_img: 'assets/images/service-list.png'
};
