
export let EcommerceContent = {
  heading: 'A Simple solution works for you',
  texts: [
    { text: '<p>E-Commerce is changing rapidly. With new technology, new cyber threats, the industry evolves at a great pace year on year.</p>' },
    { text: '<p>We make things simple for our customers, so they can concentrate on their business and we support them 24/7 when they need whatever they need. </p>' },
    { text: '<p>As a true digital partner, we are working on our Best “All in one” cloud base e-commerce solution so that you can have everything house.</p>' }
  ],
  items: [
    { item: 'Social media '},
    { item: 'Mobile app development'},
    { item: 'E-commerce solution'},
    { item: '1-month FREE social media boosting'},
    { item: 'Branding company portofolio'},
    { item: 'Graphic designing '},
  ],
  content_list: '',
  content_img: 'assets/images/service-list.png'
};
