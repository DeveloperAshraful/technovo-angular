import { Campaign } from './campaign';

export const businessCampaigns: Campaign[] = [
  {
    camp_id: 1,
    camp_title: 'Tech Novo Ltd',
    camp_image: 'assets/images/technovo/technovo-1.png',
    camp_url: 'https://www.facebook.com/technovoltd'
  },
  {
    camp_id: 2,
    camp_title: 'Tech Novo Ltd',
    camp_image: 'assets/images/technovo/technovo-2.png',
    camp_url: 'https://www.facebook.com/technovoltd'
  },
  {
    camp_id: 3,
    camp_title: 'Tech Novo Ltd',
    camp_image: 'assets/images/technovo/technovo-3.png',
    camp_url: 'https://www.facebook.com/technovoltd'
  }
];
