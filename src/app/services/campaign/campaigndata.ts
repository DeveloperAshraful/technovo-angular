import { Campaign } from './campaign';

export const CampaignData: Campaign[] = [
  {
    camp_id: 1,
    camp_title: 'Indian Hitchin',
    camp_image: 'assets/images/campaign/campaign-14.png',
    camp_url: 'https://www.facebook.com/Indianhitchin'
  },
  {
    camp_id: 2,
    camp_title: 'Flava',
    camp_image: 'assets/images/campaign/campaign-12.png',
    camp_url: 'https://www.facebook.com/FlavaLuton'
  },
  {
    camp_id: 3,
    camp_title: 'Baltistan',
    camp_image: 'assets/images/campaign/campaign-8.png',
    camp_url: 'https://www.facebook.com/Baltistantakeway'
  },
  {
    camp_id: 4,
    camp_title: 'Indian Hitchin',
    camp_image: 'assets/images/campaign/campaign-13.png',
    camp_url: 'https://www.facebook.com/Indianhitchin'
  },
  {
    camp_id: 5,
    camp_title: 'Flava',
    camp_image: 'assets/images/campaign/campaign-11.png',
    camp_url: 'https://www.facebook.com/FlavaLuton'
  },
  {
    camp_id: 6,
    camp_title: 'Indian Hitchin',
    camp_image: 'assets/images/campaign/campaign-10.png',
    camp_url: 'https://www.facebook.com/Indianhitchin'
  },
  {
    camp_id: 7,
    camp_title: 'Flava',
    camp_image: 'assets/images/campaign/campaign-1.png',
    camp_url: 'https://www.facebook.com/FlavaLuton'
  },
  {
    camp_id: 8,
    camp_title: 'Flava',
    camp_image: 'assets/images/campaign/campaign-2.png',
    camp_url: 'https://www.facebook.com/FlavaLuton'
  },
  {
    camp_id: 9,
    camp_title: 'Flava',
    camp_image: 'assets/images/campaign/campaign-3.png',
    camp_url: 'https://www.facebook.com/FlavaLuton'
  },
  {
    camp_id: 10,
    camp_title: 'Flava',
    camp_image: 'assets/images/campaign/campaign-4.png',
    camp_url: 'https://www.facebook.com/FlavaLuton'
  },
  {
    camp_id: 11,
    camp_title: 'Indian Hitchin',
    camp_image: 'assets/images/campaign/campaign-5.png',
    camp_url: 'https://www.facebook.com/Indianhitchin'
  },
  {
    camp_id: 12,
    camp_title: 'Indian Hitchin',
    camp_image: 'assets/images/campaign/campaign-6.png',
    camp_url: 'https://www.facebook.com/Indianhitchin'
  },
  {
    camp_id: 13,
    camp_title: 'Indian Hitchin',
    camp_image: 'assets/images/campaign/campaign-7.png',
    camp_url: 'https://www.facebook.com/Indianhitchin'
  },
  {
    camp_id: 14,
    camp_title: 'Indian Hitchin',
    camp_image: 'assets/images/campaign/campaign-9.png',
    camp_url: 'https://www.facebook.com/Indianhitchin'
  },
  {
    camp_id: 15,
    camp_title: 'Indian Hitchin',
    camp_image: 'assets/images/campaign/campaign-15.png',
    camp_url: 'https://www.facebook.com/Indianhitchin'
  }
];
