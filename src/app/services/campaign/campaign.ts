export interface Campaign {
  camp_id: number;
  camp_title: string;
  camp_url: string;
  camp_image: string;
}
