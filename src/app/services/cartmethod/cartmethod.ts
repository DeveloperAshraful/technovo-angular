export interface Cartmethod {
  method_id: number;
  method_name: string;
  method_image: string;
  method_link: string;
}
