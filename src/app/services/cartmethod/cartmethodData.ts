import {Cartmethod} from "./cartmethod";

export const CartmethodData: Cartmethod[] = [
  {
    method_id: 1,
    method_name: 'Shopware',
    method_image: 'assets/images/brands/Shopware.png',
    method_link: 'https://www.shopware.com/en/'
  },
  {
    method_id: 2,
    method_name: 'Smart',
    method_image: 'assets/images/brands/SMART.png',
    method_link: ''
  },
  {
    method_id: 3,
    method_name: 'Virtue Mart',
    method_image: 'assets/images/brands/Virtue-mart.png',
    method_link: 'https://virtuemart.net/'
  },
  {
    method_id: 4,
    method_name: 'Uber Cart',
    method_image: 'assets/images/brands/uber-cart.png',
    method_link: ''
  },
  {
    method_id: 5,
    method_name: 'Woo Commerce',
    method_image: 'assets/images/brands/woo-commerce.png',
    method_link: 'https://woocommerce.com/'
  },
  {
    method_id: 6,
    method_name: 'X Cart',
    method_image: 'assets/images/brands/X-Cart.png',
    method_link: 'https://www.x-cart.com/'
  },
  {
    method_id: 7,
    method_name: 'Xero',
    method_image: 'assets/images/brands/Xero.png',
    method_link: 'https://www.xero.com/'
  },
  {
    method_id: 8,
    method_name: 'Zen Cart',
    method_image: 'assets/images/brands/Zen-Cart.png',
    method_link: 'https://www.zen-cart.com/'
  }
]
