export interface Feature {
  feature_id: number;
  feature_title: string;
  feature_image: string;
  feature_link: string;
}
