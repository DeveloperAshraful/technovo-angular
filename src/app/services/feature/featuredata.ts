import {Feature} from "./feature";

export const FeatureData: Feature[] = [
  {
    feature_id: 1,
    feature_title: 'Mobile Order',
    feature_image: 'assets/images/novopay/card-machine.png',
    feature_link: 'https://novopay.uk/contactless/'
  },
  {
    feature_id: 2,
    feature_title: 'Online payment',
    feature_image: 'assets/images/novopay/online-payment.png',
    feature_link: 'https://novopay.uk/payment-gateway/'
  },
  {
    feature_id: 3,
    feature_title: 'Integrated EPOS system',
    feature_image: 'assets/images/novopay/phone-payment.png',
    feature_link: 'https://novopay.uk/epos-system/'
  }

]
