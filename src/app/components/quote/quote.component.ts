import { Component, OnInit } from '@angular/core';
import { faBriefcase, faCommentDots, faEnvelope, faPhone, faUser } from '@fortawesome/free-solid-svg-icons';
import { FormControl, Validators } from "@angular/forms";
import { environment } from "../../../environments/environment";
import { ContactService } from "../../services/contact/contact.service";

@Component({
  selector: 'app-quote',
  templateUrl: './quote.component.html',
  styleUrls: ['./quote.component.css']
})
export class QuoteComponent implements OnInit {

  user = faUser; briefcase = faBriefcase; phone = faPhone; envelope = faEnvelope; message = faCommentDots;

  constructor( public http: ContactService) { }

  private SendMail = environment.baseURI;

  submitted = false;
  btnText = "Get a free quote";

  ngOnInit(): void { }

  nameFormControl = new FormControl('', [
    Validators.required
  ]);
  businessFormControl = new FormControl('', [
    Validators.required
  ]);
  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")
  ]);
  phoneFormControl = new FormControl('', [
    Validators.required
  ]);
  messageFormControl = new FormControl('', [
    Validators.required
  ]);

  submitQuote(): void {

    this.submitted = true;
    this.btnText = 'Sending....';

    let messages = '';
    messages += '<p><strong>Name: </strong>' + this.nameFormControl.value + '</p>';
    messages += '<p><strong>Business: </strong>'+ this.businessFormControl.value +'</p>';
    messages += '<p><strong>Email: </strong>'+ this.emailFormControl.value +'</p>';
    messages += '<p><strong>Phone: </strong>'+ this.phoneFormControl.value +'</p>';
    messages += '<p><strong>Message: </strong><br/>'+ this.messageFormControl.value +'</p>';

    let admin = {
      to: 'hello@tech-novo.uk',
      from: 'hello@tech-novo.uk',
      subject: 'Tech Novo Contact Form',
      content: messages,
      CC: 'hello@tech-novo.uk',
      BCC: 'fosiul@gmail.com'
    }

    // Admin Mail
    this.http.sendEmail(this.SendMail +'/connector/mail',admin).subscribe(
      data => {
        let res:any = data;
        //console.log('Quote send has been successfully !!!');

        this.btnText = 'Get a free quote';
        this.nameFormControl.reset();
        this.businessFormControl.reset();
        this.emailFormControl.reset();
        this.phoneFormControl.reset();
        this.messageFormControl.reset();

        setTimeout(()=>{
          this.submitted = false;
        }, 5000);

      },
      err => {
        console.log(err);
      }
    );

  }

}
