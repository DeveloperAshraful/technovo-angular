import { Component, OnInit } from '@angular/core';
import {NewsData} from '../../services/news/newsData';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css']
})
export class NewsComponent implements OnInit {

  heading = 'Read our latest news and features...';
  newsItem = NewsData;

  constructor() { }

  ngOnInit(): void {

  }


}
