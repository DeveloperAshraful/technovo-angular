import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-shareholder',
  templateUrl: './shareholder.component.html',
  styleUrls: ['./shareholder.component.css']
})
export class ShareholderComponent implements OnInit {

  novopay = 'assets/images/novopay-logo.png';
  novopay_url = 'https://novopay.uk/';
  technovo = 'assets/images/logo.png';
  technovo_url = 'javascript:void(0)';

  constructor() { }

  ngOnInit(): void {
  }

}
