import { Component, OnInit, Input } from '@angular/core';
import {faCheck} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.css']
})
export class ContentComponent implements OnInit {

  check = faCheck;

  @Input() contents;

  constructor() { }

  ngOnInit(): void {
  }

}
