import { Component, OnInit } from '@angular/core';
import { faChevronRight} from "@fortawesome/free-solid-svg-icons";
import {FeatureData} from "../../services/feature/featuredata";

@Component({
  selector: 'app-feature',
  templateUrl: './feature.component.html',
  styleUrls: ['./feature.component.css']
})
export class FeatureComponent implements OnInit {

  nextArrow = faChevronRight;
  features = FeatureData;

  constructor() { }

  ngOnInit(): void {
  }

}
