import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from "@angular/forms";
import { environment } from "../../../environments/environment";
import { ContactService } from "../../services/contact/contact.service";

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  info_address = 'Innovation Centre & Business Base 110 Butterfield <br> Great Marlings, Luton, LU2 8DL';
  info_email =  'hello@tech-novo.uk';
  info_phone = '03301139963';

  zoom: number = 15;
  lat = 51.9106339;
  lng = -0.38941;

  constructor( public http: ContactService ) { }

  private SendMail = environment.baseURI;

  submitted = false;
  btnText = "Send";

  ngOnInit(): void { }

  nameFormControl = new FormControl('', [
    Validators.required
  ]);
  businessFormControl = new FormControl('', [
    Validators.required
  ]);
  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")
  ]);
  subjectFormControl = new FormControl('', [
    Validators.required
  ]);
  messageFormControl = new FormControl('', [
    Validators.required
  ]);


  submitForm(): void {

    this.submitted = true;
    this.btnText = 'Sending....';

    let contents = {
      Name: this.nameFormControl.value,
      Business: this.businessFormControl.value,
      Email: this.emailFormControl.value,
      Subject: this.subjectFormControl.value,
      Message: this.messageFormControl.value,
    }

    let messages = '';
    messages += '<p><strong>Name: </strong>' + contents.Name + '</p>';
    messages += '<p><strong>Phone Number: </strong>'+ contents.Business +'</p>';
    messages += '<p><strong>Email: </strong>'+ contents.Email +'</p>';
    messages += '<p><strong>Subject: </strong>'+ contents.Subject +'</p>';
    messages += '<p><strong>Message: </strong><br/>'+ contents.Message +'</p>';

    let admin = {
      to: 'hello@tech-novo.uk',
      from: 'hello@tech-novo.uk',
      subject: 'Tech Novo Contact Form',
      content: messages,
      CC: 'hello@tech-novo.uk',
      BCC: 'fosiul@gmail.com'
    }

    // Admin Mail
    this.http.sendEmail(this.SendMail +'/connector/mail',admin).subscribe(
      data => {
        let res:any = data;
        //console.log('Quote send has been successfully !!!');

        this.btnText = 'Send';
        this.nameFormControl.reset();
        this.businessFormControl.reset();
        this.emailFormControl.reset();
        this.subjectFormControl.reset();
        this.messageFormControl.reset();

        setTimeout(()=>{
          this.submitted = false;
        }, 5000);

      },
      err => {
        console.log(err);
      }
    );

  }

}


