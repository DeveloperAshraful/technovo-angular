import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CartmethodComponent } from './cartmethod.component';

describe('CartmethodComponent', () => {
  let component: CartmethodComponent;
  let fixture: ComponentFixture<CartmethodComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CartmethodComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CartmethodComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
