import { Component, OnInit } from '@angular/core';
import { OwlOptions } from "ngx-owl-carousel-o";
import {CartmethodData} from "../../services/cartmethod/cartmethodData";

@Component({
  selector: 'app-cartmethod',
  templateUrl: './cartmethod.component.html',
  styleUrls: ['./cartmethod.component.css']
})
export class CartmethodComponent implements OnInit {

  cartmethods = CartmethodData;

  cartSlides: OwlOptions = {
    loop: true,
    mouseDrag: true,
    dots: false,
    nav: true,
    margin: 5,
    stagePadding: 0,
    navSpeed: 700,
    navText: ['&#8249;', '&#8250;'],
    responsive: {
      0: {
        items: 2
      },
      400: {
        items: 3
      },
      740: {
        items: 4
      },
      940: {
        items: 5
      }
    }
  }

  constructor() { }

  ngOnInit(): void {
  }



}
