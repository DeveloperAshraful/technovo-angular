import { Component, OnInit } from '@angular/core';
import {ProgressData} from '../../services/progress/progressData';

@Component({
  selector: 'app-progress',
  templateUrl: './progress.component.html',
  styleUrls: ['./progress.component.css']
})
export class ProgressComponent implements OnInit {

  progress = ProgressData;

  constructor() { }

  ngOnInit(): void {
  }

}
