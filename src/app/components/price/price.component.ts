import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-pricelist',
  templateUrl: './price.component.html',
  styleUrls: ['./price.component.css']
})
export class PricelistComponent implements OnInit {

  @Input() prices;

  public showList: Boolean = false;

  constructor() { }

  ngOnInit(): void {
  }

}
