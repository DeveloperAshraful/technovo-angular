import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-callaction',
  templateUrl: './callaction.component.html',
  styleUrls: ['./callaction.component.css']
})
export class CallactionComponent implements OnInit {

  callTitle = 'Simplifying your digital experience';
  callBtn = 'Get a free quote';
  callUrl = '/contact-us';

  constructor() { }

  ngOnInit(): void {
  }

}
