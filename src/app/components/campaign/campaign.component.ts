import { Component, OnInit, Input } from '@angular/core';
import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-campaign',
  templateUrl: './campaign.component.html',
  styleUrls: ['./campaign.component.css'],
  providers: [NgbModalConfig, NgbModal]
})
export class CampaignComponent implements OnInit {

  @Input() campaigns;

  selectedCampaign;

  constructor(config: NgbModalConfig, private modalService: NgbModal) {
    config.backdrop = 'static';
    config.backdropClass = 'campModal';
    config.keyboard = false;
    config.centered = true;
    config.size = 'lg';
    config.windowClass = 'yuma-campaign-modal';
  }

  ngOnInit(): void { }

  open(campModal, camp) {
    this.modalService.open(campModal);
    this.selectedCampaign = {
      id: camp.camp_id,
      title: camp.camp_title,
      image: camp.camp_image,
      url: camp.camp_url
    };
  }


}
