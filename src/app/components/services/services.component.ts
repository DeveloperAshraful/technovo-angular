import { Component, OnInit } from '@angular/core';
import {faChevronRight} from '@fortawesome/free-solid-svg-icons';
import { ServicesData } from '../../services/services/servicesData';

@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.css']
})
export class ServicesComponent implements OnInit {

  cheRight = faChevronRight;

  services = ServicesData;

  constructor() { }

  ngOnInit(): void {
  }

}
