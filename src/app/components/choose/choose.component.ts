import { Component, OnInit } from '@angular/core';
import {ChooseData} from '../../services/choose/chooseData';

@Component({
  selector: 'app-why-choose',
  templateUrl: './choose.component.html',
  styleUrls: ['./choose.component.css']
})
export class WhyChooseComponent implements OnInit {

  heading = 'Why choose Tech Novo?';
  chooses = ChooseData;

  constructor() { }

  ngOnInit(): void {
  }

}
