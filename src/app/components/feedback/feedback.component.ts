import { Component, OnInit } from '@angular/core';
import {ContactService} from '../../services/contact/contact.service';
import {environment} from '../../../environments/environment';
import {FormControl, Validators} from "@angular/forms";

@Component({
  selector: 'app-feedback-form',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.css']
})
export class FeedbackComponent implements OnInit {

  logo = 'https://tech-novo.uk/assets/images/logo.png';

  constructor( public http: ContactService ) { }

  private SendMail = environment.baseURI;

  submitted = false;
  btnText = "Send";

  ngOnInit(): void { }

  nameFormControl = new FormControl('', [
    Validators.required
  ]);
  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")
  ]);


  submitForm(): void {

    this.submitted = true;
    this.btnText = 'Sending....';

    let contents = {
      Name: this.nameFormControl.value,
      Email: this.emailFormControl.value
    }

    let messages = `
      <!DOCTYPE html>
      <html lang="en">
      <head>
          <meta charset="utf-8">
          <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
          <title>Welcome TechNovo Ltd</title>
              <style>
                  @media screen and (max-width: 500px) {
                      footer {
                          padding-bottom: 30px !important;
                      }
                      footer table tr td {
                          width: 100%;
                          display: block;
                          padding-left: 0 !important;
                          padding-right: 0 !important;
                          padding-bottom: 0 !important;
                          text-align: center;
                      }
                  }
              </style>
      </head>
      <body style="background: #efefef;margin: 10px;border: 1px solid #eeeeee;font-family: 'Open Sans', sans-serif;font-size: 90%;">
        <div style="max-width:700px;margin:0 auto;background:#fff;box-shadow: 0 0 10px rgba(0, 0, 0, .15);">
            <div style="text-align:center;padding: 20px 15px;background: #f5f5f5;">
                <a target="_blank" href="https://tech-novo.uk/">
                    <img style="max-width: 150px;" src="https://tech-novo.uk/assets/images/logo.png">
                </a>
            </div>
            <div style="padding: 10px 30px 20px;">
                <h4 style="font-size: 16px;margin-bottom: 0;">Dear <strong>${ contents.Name }</strong></h4>
                <p style="line-height: 27px;font-size: 14px;color: #333;">We would like to take this opportunity to thank you for taking our services.</p>
                <p style="line-height: 27px;font-size: 14px;color: #333;">We hope we have exceeded your expectations. Can you be kind enough to share your feedback on Trustpilot?  An automated email from Trustpilot will arrive shortly.</p>

                <p style="line-height: 27px;font-size: 14px;color: #333;">As a way of saying a big thank you, we’re offering a 10% discount on your next purchase with us.</p>
                <p style="line-height: 27px;font-size: 14px;color: #333;"><strong>Many thanks</strong>
                    <br /><strong>Tech Novo Team</strong>
                </p>
            </div>
            <footer style="background: #F6F6F6;padding: 0;border: 0;">
                <table cellpadding="0" cellspacing="0" style="width: 100%">
                    <tr>
                        <td style="padding: 30px;vertical-align: top;"><a target="_blank" href="https://tech-novo.uk/">
                                <img style="max-width: 120px;" src="https://tech-novo.uk/assets/images/logo.png" />
                            </a>
                            <p style="line-height: 25px;font-size: 14px;margin-bottom: 0;">Contact: 03301139963
                                <br />Email: <a style="color: #333;" href="mailto:hello@tech-novo.uk">hello@tech-novo.uk</a>
                            </p>
                        </td>
                        <td style="padding: 30px 0;vertical-align: top;">
                            <h5 style="margin: 0;font-size: 16px">Information</h5>
                            <ul style="padding: 0;list-style: none;line-height: 25px;margin-bottom: 0;">
                                <li style="margin: 0;"><a style="text-decoration: none;color: #333;font-size: 14px;"
                                        href="https://tech-novo.uk/">Home</a></li>
                                <li style="margin: 0;"><a style="text-decoration: none;color: #333;font-size: 14px;"
                                        href="https://tech-novo.uk/web-design">Web Design</a>
                                </li>
                                <li style="margin: 0;"><a style="text-decoration: none;color: #333;font-size: 14px;"
                                        href="https://tech-novo.uk/ecommerce">E-Commerce</a></li>
                                <li style="margin: 0;"><a style="text-decoration: none;color: #333;font-size: 14px;"
                                        href="https://tech-novo.uk/social-media-pricing">Social Media Pricing</a></li>
                                <li style="margin: 0;"><a style="text-decoration: none;color: #333;font-size: 14px;"
                                        href="https://tech-novo.uk/social-media-restaurant-takeaways">Our Social media Work</a>
                                </li>
                                <li style="margin: 0;"><a style="text-decoration: none;color: #333;font-size: 14px;"
                                        href="https://tech-novo.uk/about-us">About Us</a></li>
                                <li style="margin: 0;"><a style="text-decoration: none;color: #333;font-size: 14px;"
                                        target="_blank" href="https://tech-novo.uk/contact-us">Contact Us</a>
                                </li>
                            </ul>
                        </td>
                        <td style="padding: 30px;vertical-align: top;">
                            <h5 style="margin: 0;font-size: 16px;">Connect With Us</h5>
                            <ul style="padding: 0;list-style: none;line-height: 25px;">
                                <li style="display: inline-block; padding:0 5px 0 0;margin:0;"><a target="_blank"
                                        href="https://www.facebook.com/technovoltd"><img style="max-width: 18px;"
                                            src="https://tech-novo.uk/assets/images/facebook.png" /></a>
                                </li>
                                <li style="display: inline-block; padding:0 5px 0 0;margin:0;"><a target="_blank"
                                        href="https://twitter.com/technovoltd"><img style="max-width: 18px;"
                                            src="https://tech-novo.uk/assets/images/twitter.png" /></a>
                                </li>
                                <li style="display: inline-block; padding:0 5px 0 0;margin:0;"><a target="_blank"
                                        href="https://www.instagram.com/technovoltd/"><img style="max-width: 18px;"
                                            src="https://tech-novo.uk/assets/images/instragram.png" /></a>
                                </li>
                                <li style="display: inline-block; padding:0 5px 0 0;margin:0;"><a target="_blank"
                                        href="mailto:hello@tech-novo.uk"><img style="max-width: 18px;"
                                            src="https://tech-novo.uk/assets/images/email.png" /></a>
                                </li>
                            </ul>
                        </td>
                    </tr>
                </table>
            </footer>
           </div>
        </body>
      </html>
    `;

    let feedback = {
      to: this.emailFormControl.value,
      from: 'hello@tech-novo.uk',
      subject: 'Give us a review for a 10% discount',
      content: messages,
      CC: 'hello@tech-novo.uk',
      BCC: 'tech-novo.uk+27ce094382@invite.trustpilot.com',
      from_name:'Tech Novo Team'
    }

    // Admin Mail
    this.http.sendEmail(this.SendMail +'/connector/mail',feedback).subscribe(
      data => {
        let res:any = data;
        console.log('Quote send has been successfully !!!');

        this.btnText = 'Send';
        this.nameFormControl.reset();
        this.emailFormControl.reset();

        setTimeout(()=>{
          this.submitted = false;
        }, 5000);

      },
      err => {
        console.log(err);
      }
    );

  }

}
