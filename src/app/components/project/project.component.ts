import { Component, OnInit } from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';
import {ProjectData} from '../../services/project/projectData';

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.css']
})

export class ProjectComponent implements OnInit {

  constructor() { }

  projectTitle = 'Recent Projects';
  projectAll = '';
  projects = ProjectData;

  projectSlide: OwlOptions = {
    loop: true,
    mouseDrag: true,
    stagePadding: 0,
    margin: 0,
    touchDrag: true,
    pullDrag: true,
    dots: false,
    nav: true,
    navSpeed: 700,
    navText: ['&#8249;', '&#8250;'],
    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 2
      },
      740: {
        items: 3
      },
      940: {
        items: 4
      }
    }
  };

  ngOnInit(): void {
  }

}
