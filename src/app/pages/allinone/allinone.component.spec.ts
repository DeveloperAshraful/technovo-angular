import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PageAllinComponent } from './allinone.component';

describe('PageAllinComponent', () => {
  let component: PageAllinComponent;
  let fixture: ComponentFixture<PageAllinComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PageAllinComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PageAllinComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
