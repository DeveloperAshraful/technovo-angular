import { Component, OnInit } from '@angular/core';
import {AllinoneBanner} from "../../services/banner/allinonebanner";
import {AllContent} from "../../services/content/allcontent";

@Component({
  selector: 'app-page-allinone',
  templateUrl: './allinone.component.html',
  styleUrls: ['./allinone.component.css']
})
export class PageAllinComponent implements OnInit {

  allinoneBanner = AllinoneBanner;
  allContents = AllContent;


  constructor() { }

  ngOnInit(): void {
  }

}
