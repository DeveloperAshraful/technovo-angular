import { Component, OnInit } from '@angular/core';
import {DesignBanner} from '../../services/banner/designbanner';
import {DesignPrice} from '../../services/price/designpricing';
import {DesignContent} from '../../services/content/designcontent';

@Component({
  selector: 'app-page-webdesign',
  templateUrl: './webdesign.component.html',
  styleUrls: ['./webdesign.component.css']
})
export class PageWebDesignComponent implements OnInit {

  designBanners = DesignBanner;
  designPrices = DesignPrice;
  designContents = DesignContent;

  constructor() { }

  ngOnInit(): void {
  }

}
