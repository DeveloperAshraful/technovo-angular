import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PageWebDesignComponent } from './webdesign.component';

describe('PageWebDesignComponent', () => {
  let component: PageWebDesignComponent;
  let fixture: ComponentFixture<PageWebDesignComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PageWebDesignComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PageWebDesignComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
