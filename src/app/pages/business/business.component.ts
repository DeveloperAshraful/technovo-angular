import { Component, OnInit } from '@angular/core';
import {BusinessBanner} from '../../services/banner/businesslbanner';
import {businessCampaigns} from "../../services/campaign/businesscampaign";

@Component({
  selector: 'app-page-business',
  templateUrl: './business.component.html',
  styleUrls: ['./business.component.css']
})
export class PageBusinessComponent implements OnInit {

  businessBanners = BusinessBanner;
  businessCampaign = businessCampaigns;

  constructor() { }

  ngOnInit(): void {
  }

}
