import { Component, OnInit } from '@angular/core';
import {AboutContent} from "../../services/content/aboutcontent";
import {AboutBanner} from "../../services/banner/aboutbanner";

@Component({
  selector: 'app-page-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class PageAboutComponent implements OnInit {

  AboutBanners = AboutBanner;
  AboutContents = AboutContent;

  constructor() { }

  ngOnInit(): void {
  }

}
