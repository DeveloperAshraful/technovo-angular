import { Component, OnInit } from '@angular/core';
import {HomePricing} from '../../services/price/homepricing';

@Component({
  selector: 'app-page-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class PageHomeComponent implements OnInit {

  homePrices = HomePricing;

  constructor() { }

  ngOnInit(): void {
  }

}
