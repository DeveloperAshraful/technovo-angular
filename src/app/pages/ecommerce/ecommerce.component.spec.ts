import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PageEcommerceComponent } from './ecommerce.component';

describe('PageEcommerceComponent', () => {
  let component: PageEcommerceComponent;
  let fixture: ComponentFixture<PageEcommerceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PageEcommerceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PageEcommerceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
