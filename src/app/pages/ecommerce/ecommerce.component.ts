import { Component, OnInit } from '@angular/core';
import {EcommerceBanner} from '../../services/banner/ecommercebanner';
import {EcommercePrice} from '../../services/price/ecommercepricing';
import {EcommerceContent} from '../../services/content/ecommercecontent';

@Component({
  selector: 'app-page-ecommerce',
  templateUrl: './ecommerce.component.html',
  styleUrls: ['./ecommerce.component.css']
})
export class PageEcommerceComponent implements OnInit {

  ecommerceBanners = EcommerceBanner;
  ecommercePrices = EcommercePrice;

  ecommerceContents = EcommerceContent;

  constructor() { }

  ngOnInit(): void {
  }

}
