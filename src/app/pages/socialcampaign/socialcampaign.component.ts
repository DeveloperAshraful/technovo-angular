import { Component, OnInit } from '@angular/core';
import {CampaignBanner} from '../../services/banner/campaignbanner';
import {CampaignData} from '../../services/campaign/campaigndata';

@Component({
  selector: 'app-page-socialcampaign',
  templateUrl: './socialcampaign.component.html',
  styleUrls: ['./socialcampaign.component.css']
})
export class PageSocialCampaignComponent implements OnInit {

  campaignBanners = CampaignBanner;
  campaignItem = CampaignData;


  constructor() { }

  ngOnInit(): void {
  }

}
