import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PageSocialCampaignComponent } from './socialcampaign.component';

describe('PageSocialCampaignComponent', () => {
  let component: PageSocialCampaignComponent;
  let fixture: ComponentFixture<PageSocialCampaignComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PageSocialCampaignComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PageSocialCampaignComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
