import { Component, OnInit } from '@angular/core';
import {OfferData} from '../../services/offers/offerData';
import {offerBanner} from '../../services/banner/offerbanner';

@Component({
  selector: 'app-page-offer',
  templateUrl: './offer.component.html',
  styleUrls: ['./offer.component.css']
})
export class PageOfferComponent implements OnInit {

  offers = OfferData;

  offerBanner = offerBanner;

  constructor() {
  }

  ngOnInit(): void {
  }

}
