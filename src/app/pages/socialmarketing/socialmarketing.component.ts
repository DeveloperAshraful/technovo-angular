import { Component, OnInit } from '@angular/core';
import {SocialBanner} from '../../services/banner/socialbanner';
import {SocialPrice} from '../../services/price/socialpricing';
import {SocialContent} from '../../services/content/socialcontent';

@Component({
  selector: 'app-page-socialmarketing',
  templateUrl: './socialmarketing.component.html',
  styleUrls: ['./socialmarketing.component.css']
})
export class PageSocialMarketingComponent implements OnInit {

  socialBanners = SocialBanner;
  socialPrices = SocialPrice;
  socialContents = SocialContent;

  constructor() { }

  ngOnInit(): void {
  }

}
