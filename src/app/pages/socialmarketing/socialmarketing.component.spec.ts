import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PageSocialMarketingComponent } from './socialmarketing.component';

describe('PageSocialMarketingComponent', () => {
  let component: PageSocialMarketingComponent;
  let fixture: ComponentFixture<PageSocialMarketingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PageSocialMarketingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PageSocialMarketingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
