import { Component, OnInit } from '@angular/core';
import {ProjectBanner} from '../../services/banner/projectbanner';

@Component({
  selector: 'app-page-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.css']
})
export class PageProjectsComponent implements OnInit {

  projectBanners = ProjectBanner;

  constructor() { }

  ngOnInit(): void {
  }

}
